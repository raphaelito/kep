import csv

import numpy as np


def read_dat_file(file_path, delimiter=",", verbose=False):
    """ read_dat_file
    Retourne un tableau numpy n x n rempli de 0 sauf sur la
    diagonale où on aura les PRA. Si un donneur est altruiste,
    il aura un PRA = -1.
    """
    # We count how many pairs we have
    line_count = 0
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        for _ in csv_reader:
            line_count += 1
    n = line_count - 1  # We remove first line in count
    data_array = np.zeros((n, n))

    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                if verbose: print(f'Column names are {", ".join(row)}')
            else:
                data_array[int(row[0]) - 1][int(row[0]) - 1] = float(row[4]) if int(row[6]) == 0 else -1
                altruist = "This is an altruist" if int(row[6]) == 1 else ""
                if verbose: print(f"Pair {row[0]}: Patient is '{row[1]}', Donor is '{row[2]}'.\
                PRA% is {row[4]}. {altruist}")
            line_count += 1
    if verbose: print(f'Processed {line_count} lines.')
    return data_array


def read_wmd_file(file_path, dat_data, delimiter=",", verbose=False):
    """ read_wmd_file
    Complète le tableau généré par read_dat_file, en mettant un 1 en i-1, j-1
    si le receveur i est compatible avec le donneur j.
    """
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                lines_to_skip = int(row[0])
            elif line_count <= lines_to_skip:
                pass
            else:
                if verbose: print(f"line {line_count} : {row[0]}, {row[1]}, {row[2]}")
                dat_data[int(row[0])][int(row[1])] = 1
            line_count += 1
    return dat_data


if __name__ == '__main__':
    # You can run directly parser.py to test it on some data files
    data_dat = read_dat_file('data/MD-00001-00000137.dat', verbose=False)
    data_dat = read_wmd_file('data/MD-00001-00000137.wmd', data_dat, verbose=False)
    print("data:", data_dat)

    # test : Can donor 12 can give to reciever 15 ?
    reciever = 15
    donor = 12
    print(data_dat[reciever - 1][donor - 1] == 1)
