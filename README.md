# KEP

A solver for the well known Kidney Exchange Problem

## How to use

1. git clone this repository.
2. Execute `solver.py` with the correct arguments. Some examples are:

```bash=
python3 solver.py -h # prints the help menu
python3 solver.py -l # prints the files in the data folder in the format required when passing argument
python3 solver.py -p 0.1 -d data/MD-00001-00000044 # launch the solver with a custom success probability.
```

Some data files are already included in the `data/` folder, the `-l` option prints all the files available in this folder.
`-p` is used to specify a minimum threshold of acceptation of an operation of a couple of operations.
