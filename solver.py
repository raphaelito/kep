import argparse

import parser


def diag_to_tuple(pairs_matrix):
    """ From the wmd matrix, creates a list of all pairs with their index and their PRA """
    pairs = list()
    for i in range(len(pairs_matrix)):
        pairs.append([i, pairs_matrix[i][i]])
    return pairs


def find_compatibles(sorted, pair, wmd):
    """
    Given a pair, and a list of all pairs sorted by their PRAs, returns the first compatible pair with the best PRA
    """
    res = list()
    for index, donor in enumerate(sorted):  # [[N° paire, pra], ... ]
        if donor[0] == pair:
            pass
        elif wmd[donor[0]][pair] and wmd[pair][donor[0]]:
            return donor, index
    return None, None


def solve(sorted_pairs, matrix):
    """ From a list of all pairs sorted by their PRAs, creates the schedule of operations. """
    current = 0
    res = list()
    while len(sorted_pairs) != 0 and len(sorted_pairs) > current and sorted_pairs[-1] != sorted_pairs[current] and \
            sorted_pairs[current][1] != -1:
        pair = sorted_pairs[current]
        compatible, index_compatible = find_compatibles(sorted_pairs, pair[0], matrix)
        if compatible is not None:
            res.append([pair, compatible])
            sorted_pairs.pop(index_compatible)
            sorted_pairs.pop(current)
        else:
            current += 1
    return res


def matprint(mat, fmt="g"):
    """ Pretty prints the wmd matrix to enhance its readability """
    col_maxes = [max([len(("{:" + fmt + "}").format(x)) for x in col]) for col in mat.T]
    for x in mat:
        for i, y in enumerate(x):
            print(("{:" + str(col_maxes[i]) + fmt + "}").format(y), end="  ")
        print("")


def expectation(res, threshold=0.05):
    """ Computes the expectation and removes all operations with an probabilty of sucess < threshold """
    esp = 0
    new_solution = list()
    operations = 0
    for elm in res:
        pra1 = elm[0][1]
        pra2 = elm[1][1]
        if pra1 == -1:
            new_esp = pra2  # altruist
            proba = pra2
        elif pra2 == -1:
            new_esp = pra1  # altruist
            proba = pra1
        else:
            new_esp = 2 * pra1 * pra2
            proba = pra1 * pra2
        if proba >= threshold:
            if pra1 != -1 and pra2 != -1:
                operations += 1
            operations += 1
            esp += new_esp
            new_solution.append([[elm[0][0] + 1, elm[0][1]], [elm[1][0] + 1, elm[1][1]]])
    return esp, new_solution, operations


if __name__ == '__main__':

    argument_parser = argparse.ArgumentParser(
        description="Generates a list of operation for the Kidney Exchange Problem")
    argument_parser.add_argument("-d", "--dataset", help="name of Data files", type=str, required=False)
    argument_parser.add_argument("-l", "--list", help="lists the available datasets", action='store_true',
                                 required=False)
    argument_parser.add_argument("-p", "--minimal_probability",
                                 help="specifies the minimal probabilty of success for a group of operations",
                                 type=float)
    args = argument_parser.parse_args()
    if not args.list and not args.dataset:
        argument_parser.print_usage()
    if args.list:
        import glob
        import os

        all_wmd_files = glob.glob("data/*.wmd")
        print("Available files:")
        for f in all_wmd_files:
            if os.path.exists(f[:-3] + "dat"):
                print(f[:-4])
        exit(0)

    if args.minimal_probability is None:
        threshold = 0.1
    else:
        threshold = args.minimal_probability
    print("minimal probability of success required = ", threshold)

    dat = parser.read_dat_file(args.dataset + ".dat")
    wmd = parser.read_wmd_file(args.dataset + ".wmd", dat)
    # np.savetxt('wmd.csv', wmd, delimiter=';', fmt='%.3f') # generates csv file for readibility

    unsorted_pairs = diag_to_tuple(wmd)
    sorted_pairs = sorted(unsorted_pairs, key=lambda p: p[1], reverse=True)  # sort by PRAs
    solution = solve(sorted_pairs, wmd)

    esper, new_sol, operations = expectation(solution, threshold=threshold)

    print("\nSOLUTION: ({} operations)\n".format(operations), new_sol)
    print("\nEXPECTATION=", esper)
